clear;
[result]=fmin_iv(3.66,28,30,0.05,1);% The test from Chris
% The bisection function test
[sigma_est1, iteration1] = bisection(["call";"put"],[3.66 4],[28 30],[0.05 0.05],[1 1],[30 30]);
% The Newton Method function test
[sigma_est2, iteration2] = nm_iv(["call";"put"],[3.66 4],[28 30],[0.05 0.05],[1 1],[30 30]);