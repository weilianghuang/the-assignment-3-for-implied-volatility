function [result]=fmin_iv(optpx,S,K,r,T)
%define a starting place to guess volatility
guess=0.25;
%define anonymous functions to represent black-scholes
d1=@(S,K,r,vol,T)(log(S/K)+(r+(vol^2)/2)*T)/(vol*sqrt(T)); 
d2=@(S,K,r,vol,T)(log(S/K)+(r-(vol^2)/2)*T)/(vol*sqrt(T)); 
bscallpx=@(S,K,r,vol,T)S*normcdf(d1(S,K,r,vol,T))-K*exp(-r*T)*normcdf(d2(S,K,r,vol,T));
%define an anonymous function to minimize
mindiff=@(vol)abs(optpx-bscallpx(S,K,r,vol,T));
%use fminsearch to minimize the above fn and return implied vol 
result=fminsearch(mindiff,guess);