function [sigma_est, iteration] = nm_iv(type, C, S0, r, T, K)
% [m, literation] = nm_iv(type, C, S0, r, T, K) is to return the
% implied volatility that is sigma_est and literation.
% The Input details
% S0 � Underlying price
% K  � Strike price
% r  � Risk-free rate
% T  � Tenor
% C  � Option price
% type - call or put
sigma_est = 0.5*ones(1,length(C));
for i = 1:length(C) % Exchange the string array to logical number with call equal to 1, put is 0
    if type(i) == "call"
        D(i) = 1;
    else
        D(i) = 0;
    end
end
d1 = @(S, K, sigma, r, T)(log(S./K) + (r + 0.5*sigma.^2).*T)./(sigma.*sqrt(T));
% d1(S, K, sigma, r, T)
d2 = @(S, K, sigma, r, T)d1(S, K, sigma, r, T) - (sigma.*sqrt(T));
% d2(S, K, sigma, r, T)
cpx = @(S, K, sigma, r, T)S.*normcdf(d1(S, K, sigma, r, T)) - K.*exp(-r.*T).*normcdf(d2(S, K, sigma, r, T));
% The call option price
ppx = @(S, K, sigma, r, T)K.*exp(-r.*T).*normcdf(-d2(S, K, sigma, r, T)) - S.*normcdf(-d1(S, K, sigma, r, T));
% The put option price
vega = @(S, K, sigma, r, T)S.*normpdf(d1(S, K, sigma, r, T)).*sqrt(T);
% vega(S, K, sigma, r, T)
err = 1;
% Initial value of option price minus
err1 = 1;
% Initial value of sigma_est2 - sigma_est
iteration = 0;
% Initial value of iteration
while (any(err > 1e-4) || any(err1 > 1e-4))
    sigma_est2 = sigma_est;% Set the last sigma_est
    sigma_est = sigma_est2 - (cpx(S0, K, sigma_est2, r, T).*D + ppx(S0, K, sigma_est2, r, T).*~D - C)./vega(S0, K, sigma_est2, r, T);
    % Caculate the next sigma_east value with Newton Method
    err = abs(cpx(S0, K, sigma_est, r, T).*D + ppx(S0, K, sigma_est, r, T).*~D - C);% The abs value between two option price
    err1 = abs(sigma_est2 - sigma_est);% The abs value of sigma_est2 - sigma_est
    iteration = iteration + 1;
end

end

