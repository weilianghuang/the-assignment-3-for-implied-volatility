function [sigma_est, iteration] = bisection(type, C, S0, r, T, K)
% [m, literation] = bisection(type, C, S0, r, T, K) is to return the
% implied volatility that is sigma_est and literation.
% The Input details
% S0 � Underlying price
% K  � Strike price
% r  � Risk-free rate
% T  � Tenor
% C  � Option price
% type - call or put
err = 1e-4;% The biggest error between the a & b and f1 & f2
a = -1000*ones(1,length(C));% The low boundary of sigma_est
b = 1000*ones(1,length(C));% The up boundary of sigma_est
d1 = @(S, K, sigma, r, T)(log(S./K) + (r + 0.5*sigma.^2).*T)./(sigma.*sqrt(T));
%d1(S, K, sigma, r, T)
d2 = @(S, K, sigma, r, T)d1(S, K, sigma, r, T) - (sigma.*sqrt(T));
%d2(S, K, sigma, r, T)
call_gap = @(C, S, K, sigma, r, T)S.*normcdf(d1(S, K, sigma, r, T)) - K.*exp(-r.*T).*normcdf(d2(S, K, sigma, r, T)) - C;
% The spread between the call price and call price from sigma_est.
put_gap = @(C, S, K, sigma, r, T)K.*exp(-r.*T).*normcdf(-d2(S, K, sigma, r, T)) - S.*normcdf(-d1(S, K, sigma, r, T)) - C;
% The spread between the put price and call price from sigma_est.
for i = 1:length(C) % Exchange the string array to logical number with call equal to 1, put is 0
    if type(i) == "call"
        D(i) = 1;
    else
        D(i) = 0;
    end
end

f1 = call_gap(C, S0, K, a, r, T).*D + put_gap(C, S0, K, a, r, T).*~D;
% The gap with the low boundary
f2 = call_gap(C, S0, K, b, r, T).*D + put_gap(C, S0, K, b, r, T).*~D;
% The gap with the up boundary
sigma_est = (a+b)./2;
% The sigma_est is the middle value of up and low boundary
f3 = call_gap(C, S0, K, sigma_est, r, T).*D + put_gap(C, S0, K, sigma_est, r, T).*~D;
% The gap with the sigma_est
iteration = 0*ones(1,length(C));% The initial value of literation
bigerr = f1.*f2;
bigerr(bigerr < 0) = 0;
%% This the fastest way to get the implied volatility and return the biggest literation
if (any(bigerr)) % To prevent there is no zero point
    fprintf("The interval of this function is not suitable for your numbers, please change by yourself");
else
    while(any(abs(a - b) > err) || any(abs(f1 - f2) > err))% 2 conditions of gaps of a&b and f1&f2
        md = f1.*f3;% Determine which way to do bisection
        md(md<0)=0;% from low side
        md(md>0)=1;% from up side
        b = sigma_est.*~md + b.*md;
        a = sigma_est.*md + a.*~md;
        sigma_est = (a+b)./2;
        f1 = call_gap(C, S0, K, a, r, T).*D + put_gap(C, S0, K, a, r, T).*~D;
        f2 = call_gap(C, S0, K, b, r, T).*D + put_gap(C, S0, K, b, r, T).*~D;
        f3 = call_gap(C, S0, K, sigma_est, r, T).*D + put_gap(C, S0, K, sigma_est, r, T).*~D;
        iteration = iteration + 1;% Return the biggest literation
    end
end
%% Another way to get the implied volatility and each literation but is slower

% for i = 1:length(C)
%     if(f1(i).*f3(i)<0)
%         b(i) = m(i);
%         m(i) = (a(i)+b(i))./2;
%     else
%         a(i) = m(i);
%         m(i) = (a(i)+b(i))./2;
%     end
%     err1(i) = abs(a(i)-m(i));
%     while(err1(i) > err || any(abs(f1(i) - f2(i))) > 0)
%         f1 = call_gap(C, S0, K, a, r, T).*D + put_gap(C, S0, K, a, r, T).*~D;
%         f2 = call_gap(C, S0, K, b, r, T).*D + put_gap(C, S0, K, b, r, T).*~D;
%         f3 = call_gap(C, S0, K, m, r, T).*D + put_gap(C, S0, K, m, r, T).*~D;
%         if(f1(i).*f3(i)<0)
%           b(i) = m(i);
%           m(i) = (a(i)+b(i))./2;
%         else
%           a(i) = m(i);
%           m(i) = (a(i)+b(i))./2;
%         end
%         err1(i) = abs(a(i)-b(i));
%         literation(i) = literation(i) + 1;
%     end
% end
end




  
